@extends('packages::layouts.master')
  @section('title', 'Dashboard')
    @section('header')
    <h1>Dashboard</h1>
    @stop
    @section('content') 
      @include('packages::partials.main-header')
      <!-- Left side column. contains the logo and sidebar -->
      <div class="page-container">
      @include('packages::partials.main-sidebar')
      <!-- Content Wrapper. Contains page content -->
      <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
          <div class="page-head">
              <!-- BEGIN PAGE TITLE -->
              <div class="page-title">
                  <h1>Admin Dashboard
                      <small>statistics, charts, recent events and reports</small>
                  </h1>
              </div>
              <!-- END PAGE TITLE -->

                    <!-- BEGIN PAGE BREADCRUMB -->
                    <!-- <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="index.html">Home</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span class="active">Dashboard</span>
                        </li>
                    </ul> -->
                    <!-- END PAGE BREADCRUMB -->

              <!-- BEGIN PAGE TOOLBAR -->

              <!-- END PAGE TOOLBAR -->
          </div>
        <section style="margin:15px 30px -30px 30px">
          @if(Input::has('error'))
                   <div class="alert alert-danger alert-dismissable">
                      <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                   <h4> <i class="icon fa fa-check"></i>  
                      Sorry! You are trying to access invalid URL. <a href="{{url('admin')}}"> Reset</a></h4>

                   </div>
              @endif
         <hr>  
        </section>
        @if(!Input::has('error'))
          <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <a class="dashboard-stat dashboard-stat-v2 blue" href="{{route('user')}}">
                    <div class="visual">
                        <i class="fa fa-comments"></i>
                    </div>
                    <div class="details">
                        <div class="number">
                            <span data-counter="counterup" data-value="{{ $user }}">0</span>
                        </div>
                        <div class="desc"> Total Users </div>
                    </div>
                </a>
            </div>
          </div>
        @endif 
      </div><!-- /.content-wrapper -->
     </div>
     </div>
@stop

